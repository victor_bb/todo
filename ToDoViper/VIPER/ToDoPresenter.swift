//
//  ToDoPresenter.swift
//  ToDoViper
//
//  Created by  Виктор Борисович on 26.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import Foundation

protocol ToDoPresenterInPut {
    var output: ToDoPresenterOutPut? { get set }
    
    func loadToDoList()
    func getToDoCount() -> Int
    func getToDo(at index: NSInteger) -> ToDo
    func removeToDo(at index: NSInteger)
    func addNewToDo(item: ToDo)
}

protocol ToDoPresenterOutPut: class {
    func onToDoLoad(todoList: [ToDo])
}

class ToDoPresenter: ToDoPresenterInPut {
    weak var output: ToDoPresenterOutPut?
    var interactor: ToDoInteractorInPut! = ToDoInteractor()
    var router: ToDoRouterInPut! = ToDoRouter()
    
    func getToDoCount() -> Int {
        return interactor.getToDoListCount()
    }
    
    func loadToDoList() {
        output?.onToDoLoad(todoList: interactor.getToDoList())
    }
    
    func getToDo(at index: NSInteger) -> ToDo {
        return interactor.getToDo(at: index)
    }
    
    func addNewToDo(item: ToDo) {
        interactor.addNewToDo(item: item)
    }
    
    func removeToDo(at index: NSInteger) {
        interactor.removeToDo(at: index)
    }
}

extension ToDoPresenter: ToDoInteractorOutPut, ToDoRouterOutPut {
    
}
