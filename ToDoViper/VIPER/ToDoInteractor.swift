//
//  ToDoInteractor.swift
//  ToDoViper
//
//  Created by  Виктор Борисович on 26.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import Foundation
import RealmSwift

protocol ToDoInteractorInPut {
    func getToDoListCount() -> Int
    func getToDoList() -> [ToDo]
    func getToDo(at index: NSInteger) -> ToDo
    func removeToDo(at index: NSInteger)
    func addNewToDo(item: ToDo)
}

protocol ToDoInteractorOutPut {
    
}

class ToDoInteractor: ToDoInteractorInPut {
    var todoList = [ToDo]()
    private let realm = try! Realm()
    
    func getToDo(at index: NSInteger) -> ToDo {
        return todoList[index]
    }
    
    func getToDoListCount() -> Int {
        return todoList.count
    }
    
    func getToDoList() -> [ToDo] {
        loadToDoList()
        
        return todoList
    }
    
    func addNewToDo(item: ToDo) {
        try! realm.write {
            realm.add(item)
        }
        
        todoList.insert(item, at: 0)
    }
    
    func removeToDo(at index: NSInteger) {
        let todo = todoList.remove(at: index)
        
        try! realm.write {
            realm.delete(todo)
        }
    }
    
    private func loadToDoList() {
        let todos = try! Realm().objects(ToDo.self).sorted(byKeyPath: "sort", ascending: false)
        
        if todos.count > 0 {
            
            for toDo in todos {
                todoList.append(toDo)
            }
        }
    }
}
