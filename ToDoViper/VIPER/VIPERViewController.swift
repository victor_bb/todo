//
//  VIPERViewController.swift
//  ToDoViper
//
//  Created by  Виктор Борисович on 26.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import UIKit

class VIPERViewController: UIViewController {

    @IBOutlet weak var textFieldView: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    let todoCellIdentifier = "ToDoCell"
    var presenter: ToDoPresenter!
    
    deinit {
        print("dealoc---")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
print("viewDidLoad+++")
        // Do any additional setup after loading the view.
        presenter = ToDoPresenter()
        presenter.output = self
        presenter.loadToDoList()
    }
    

    @IBAction func onSave(_ sender: UIButton) {
        if let textValue = textFieldView.text, textValue.count > 0 {
            let newToDo = ToDo()
            
            newToDo.title = textValue
            newToDo.sort = presenter.getToDoCount()
            newToDo.dateAdd = Date()
            
            presenter.addNewToDo(item: newToDo)
            
            textFieldView.text = ""
            
            tableView.reloadData()
        }
    }

}

// MARK: - VIPER realization

extension VIPERViewController: ToDoPresenterOutPut {
    func onToDoLoad(todoList: [ToDo]) {
        self.tableView.reloadData()
    }
}

// MARK: - Table realization

extension VIPERViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getToDoCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: todoCellIdentifier) as! ToDoTableViewCell
        
        let toDo = presenter.getToDo(at: indexPath.row)
        cell.label.text = toDo.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCell.EditingStyle.delete {
            // Delete the row from the data source
            presenter.removeToDo(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade);
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: true)
        tableView.setEditing(editing, animated: true)
    }
}
