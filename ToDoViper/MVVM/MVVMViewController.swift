//
//  MVVMViewController.swift
//  ToDoViper
//
//  Created by  Виктор Борисович on 26.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import UIKit

class MVVMViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = ToDoViewModel()
    let todoCellIdentifier = "ToDoCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewModel.loadToDoList()
        tableView.reloadData()
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        if let textValue = textField.text, textValue.count > 0 {
            let newToDo = ToDoMVVM()

            newToDo.title = textValue
            newToDo.sort = viewModel.getToDoCount()
            newToDo.dateAdd = Date()
            
            viewModel.add(item: newToDo)
            
            textField.text = ""

            tableView.reloadData()
        }
    }
}

// MARK: - Table realization

extension MVVMViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getToDoCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: todoCellIdentifier) as! ToDoTableViewCell
        
        let toDo = viewModel.getToDo(at: indexPath.row)
        cell.label.text = toDo.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCell.EditingStyle.delete {
            // Delete the row from the data source
            viewModel.removeToDo(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade);
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: true)
        tableView.setEditing(editing, animated: true)
    }
}
