//
//  ToDoViewModel.swift
//  ToDoViper
//
//  Created by  Виктор Борисович on 26.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import Foundation
import RealmSwift

class ToDoMVVM: Object {
    @objc dynamic var title = ""
    @objc dynamic var sort = 0
    @objc dynamic var dateAdd = Date()
}

class ToDoViewModel {
    
    var todoList = [ToDoMVVM]()
    private let realm = try! Realm()
    
    func loadToDoList() {
        let todos = try! Realm().objects(ToDoMVVM.self).sorted(byKeyPath: "sort", ascending: false)
        
        if todos.count > 0 {
            
            for toDo in todos {
                todoList.append(toDo)
            }
        }
    }
    
    func add(item: ToDoMVVM) {
        try! realm.write {
            realm.add(item)
        }
        
        todoList.insert(item, at: 0)
    }
    
    func getToDoCount() -> Int {
        return todoList.count
    }
    
    func getToDo(at index: Int) -> ToDoMVVM {
        return todoList[index]
    }
    
    func removeToDo(at index: Int) {
        
        let todo = todoList.remove(at: index)

        try! realm.write {
            realm.delete(todo)
        }
    }
}
