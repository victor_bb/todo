//
//  MVCViewController.swift
//  ToDoViper
//
//  Created by  Виктор Борисович on 25.08.2019.
//  Copyright © 2019 MT. All rights reserved.
//

import UIKit

import RealmSwift

class ToDo: Object {
    @objc dynamic var title = ""
    @objc dynamic var sort = 0
    @objc dynamic var dateAdd = Date()
}

class MVCViewController: UIViewController {
    
    @IBOutlet weak var textFieldView: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var todoList = [ToDo]()
    let todoCellIdentifier = "ToDoCell"
    private let realm = try! Realm()
    private var displayToDoLimit = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = editButtonItem;
        
        // Do any additional setup after loading the view.
        let todos = try! Realm().objects(ToDo.self).sorted(byKeyPath: "sort", ascending: false)
        
        if todos.count > 0 {
            
            for toDo in todos {
                todoList.append(toDo)
            }
            
            tableView.reloadData()
        }
    }
    
    func createLabel(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: 22)
        
        return label
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        if let textValue = textFieldView.text, textValue.count > 0 {
            let newToDo = ToDo()
            let todosCount = try! Realm().objects(ToDo.self).count
            
            newToDo.title = textValue
            newToDo.sort = todosCount
            newToDo.dateAdd = Date()
            
            try! realm.write {
                realm.add(newToDo)
            }
            
            textFieldView.text = ""
            
            todoList.insert(newToDo, at: 0)
            tableView.reloadData()
        }
    }
}

// MARK: - Table realization

extension MVCViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: todoCellIdentifier) as! ToDoTableViewCell
        
        let currentCategory = todoList[indexPath.row]
        cell.label.text = currentCategory.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCell.EditingStyle.delete {
            // Delete the row from the data source
            let todo = todoList.remove(at: indexPath.row)

            try! realm.write {
                realm.delete(todo)
            }
            
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade);
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: true)
        tableView.setEditing(editing, animated: true)
//        let status = navigationItem.leftBarButtonItem?.title
//        if status == "Edit" {
//            tableView.isEditing = true
//            navigationItem.leftBarButtonItem?.title = "Done"
//        } else {
//            tableView.isEditing = false
//            navigationItem.leftBarButtonItem?.title = "Edit"
//        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        print("moveRowAt \(sourceIndexPath) || \(destinationIndexPath)")
        let todoTarget = todoList[sourceIndexPath.row]
        let todo = todoList[destinationIndexPath.row]
        let sortA = todoTarget.sort
        print("todoTarget \(todoTarget)")
        print("todo \(todo)")
        
        todoTarget.realm?.beginWrite()
        todoTarget.sort = todo.sort
        try! todoTarget.realm?.commitWrite()
        
        todo.realm?.beginWrite()
        todo.sort = sortA
        try! todo.realm?.commitWrite()
        
        
//        let model = RealmModel()
//        model.realm?.beginWrite()
//        model.property = someValue
//        do {
//            try model.realm?.commitWrite()
//        } catch {
//            print(error.localizedDescription)
//        }
    }
    
}
